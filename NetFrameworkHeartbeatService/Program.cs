﻿using System;
using Topshelf;

namespace NetFrameworkHeartbeatService
{
    class Program
    {
        static void Main(string[] args)
        {
            var exitCode = HostFactory.Run(x =>
            {
                x.Service<HeartBeat>(s =>
                {
                    s.ConstructUsing(hearbeat => new HeartBeat());
                    s.WhenStarted(heartbeat => heartbeat.Start());
                    s.WhenStopped(heartbeat => heartbeat.Stop());
                });

                x.RunAsLocalSystem();

                x.SetServiceName("HeartBeatService");
                x.SetDisplayName("HeartBeat Service");
                x.SetDescription("Simple service that prints datetime to a file every tick of timer");
            });

            // converts 
            int exitCodeValue = (int)Convert.ChangeType(exitCode, exitCode.GetTypeCode());
            Environment.ExitCode = exitCodeValue;
            //Console.ReadLine();
        }
    }
}
